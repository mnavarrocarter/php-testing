FROM alpine:3.8

LABEL author="Matias Navarro Carter"

ENV COMPOSER_VERSION=1.7.3

# Install a host of common php extensions, along with other tools.
RUN apk add --no-cache \
    git curl make gcc unzip \
    php7 php7-zip php7-zlib \
    php7-dom php7-simplexml php7-xml php7-xmlreader php7-xmlwriter \
    php7-mcrypt php7-openssl \
    php7-gettext php7-intl \
    php7-curl php7-session \
    php7-ftp php7-fileinfo php7-iconv php7-json \
    php7-mbstring php7-ctype\
    php7-pdo php7-pdo_sqlite php7-pdo_mysql \
    php7-pear php7-phar \
    php7-posix php7-tokenizer php7-xdebug

# Enable Xdebug and test coverage support        
RUN echo "zend_extension=/usr/lib/php7/modules/xdebug.so" >> /etc/php7/php.ini \
    && echo "xdebug.coverage_enable=1" >> /etc/php7/php.ini

# Install composer for dependency installs before testing.
RUN curl --location --output /usr/local/bin/composer https://getcomposer.org/download/${COMPOSER_VERSION}/composer.phar \
    && chmod +x /usr/local/bin/composer

# What is left to do to the client is composer install on before script.

CMD ["/bin/sh"]